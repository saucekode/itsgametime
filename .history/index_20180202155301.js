//load the .env file
require('dotenv').load();
//load express framework
const express = require('express');
const app = express();
//load body parser
const bodyParser = require('body-parser');
//load request promise
const rp = require('request-promise');

//load the logger
const log = require('simple-node-logger').createSimpleFileLogger('itsgametime.log');

//get questions, answers, and correct answers
var questions = require('data/questions');
var answers = require('data/answers');
var correct_answers = require('data/correct_answers');

//Serve static files
app.use(express.static('public'));

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

//this is the homepage
app.get('/', function(req, res){
    res.send('My Paddy, ITS GAME TIME!!!');
})

//this routes to the fencer API, to check if the latitude, and longitude are inside the area
app.get('/geofence', function(req, res){
    //get fencer api key
    const FENCER_API_KEY = process.env.FENCER_API_KEY;
    //get the latitude, longitude, and accesskey from query params
    const {latitude, longitude, accessKey} = req.query;

    //set the request promise options
    const options = {
        url: `https://api.fencer.io/v1.0/position/inside/${accessKey}`,
        headers: {
            'Authorization': FENCER_API_KEY,
            'Lat-Pos': latitude,
            'Lng-Pos': longitude
        },
        json:true
    }
    rp(options)
        .then(function(fencerData){
            //return if the user is in area
            const isUserInsideArea = fencerData.data.inside;

            //respond with JSON to ChatFuel
            const outputData = {
                set_attributes: {
                    userIsInArea: isUserInsideArea
                }
            }
            res.json(outputData);
        })
        .catch(function(err){
            res.json({status: false, message: "Error occurred"});
        })
});


function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

function getNewScore(score, lastQuestionId, lastTypedAnswer){
    if(answers[lastQuestionId] == lastTypedAnswer){
        score++;
    }
    return score;
}

function processQuickReplies(replies){
    var repliesData = [];
    for(i = 0; i < replies.length; i++){
        repliesData.push({title: replies[i], block_names: ["FetchQuestion"]});
    }
    return repliesData;
}

//ChatFuel URL to fetch the questions
app.post('/getquestions', function(req, res){
    const {totalNoOfQuestions, score, totalAnsweredQuestions, lastQuestionId, lastTypedAnswer, answeredQuestions} = req.body;

    //check if answered question is recently initialized
    if(answeredQuestions == "null"){
        answeredQuestions = [];
    } else {
        answeredQuestions.push(lastQuestionId);
    }

    var ChatFuelResponse = {
        "set_attributes": {
            "answeredQuestions": answeredQuestions,
            "score": score
        },
        "messages": []
    }

    //increment the total answered questions
    if(lastQuestionId !== "null"){
        //increment total answered questions
        totalAnsweredQuestions++;

        //determine the new score
        const newScore = getNewScore(score, lastQuestionId, lastTypedAnswer);
        ChatFuelResponse.set_attributes.score = newScore;
    }
    
    //get the next question
    var questionId = getNextQuestion(answeredQuestions);
    if(questionId !== null) {
        const questionText = questions[questionId];
        const answersText = processQuickReplies(answers[questionId]);
        const newQuestion = {
            "text":  questionText,
            "quick_replies": answersText
        }
        ChatFuelResponse.messages.push(newQuestion);
    } 
    else {

    }

    //update the answered questions array
    if(answeredQuestions == "null"){
        ChatFuelResponse.set_attributes.push();
    } else {
        ChatFuelResponse.set_attributes.answeredQuestions.push({lastQuestionId});
        ChatFuelResponse.set_attributes.providedAnswers.push({lastTypedAnswer});
    }
})

app.listen(8080);