require('dotenv').load();
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const rp = require('request-promise');

//Serve static files
app.use(express.static('public'));

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

app.get('/', function(req, res){
    var FENCER_API_KEY = process.env.FENCER_API_KEY;
    const {latitude, longitude, accessKey} = req.query;
    var options = {
        url: `https://api.fencer.io/v1.0/navigation/in/${FENCER_API_KEY}`,
        headers: {
            'Authorization': FENCER_API_KEY
        }
    }
    rp(options)
        .then(function(){

        })
    res.send(FENCER_API_KEY);
});

app.listen(3000);