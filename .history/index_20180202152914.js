//load the .env file
require('dotenv').load();
//load express framework
const express = require('express');
const app = express();
//load body parser
const bodyParser = require('body-parser');
//load request promise
const rp = require('request-promise');

//load the logger
const log = require('simple-node-logger').createSimpleFileLogger('itsgametime.log');

//get questions, answers, and correct answers
var questions = require('data/questions');
var answers = require('data/answers');
var correct_answers = require('data/correct_answers');

//Serve static files
app.use(express.static('public'));

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

//this is the homepage
app.get('/', function(req, res){
    res.send('My Paddy, ITS GAME TIME!!!');
})

//this routes to the fencer API, to check if the latitude, and longitude are inside the area
app.get('/geofence', function(req, res){
    //get fencer api key
    const FENCER_API_KEY = process.env.FENCER_API_KEY;
    //get the latitude, longitude, and accesskey from query params
    const {latitude, longitude, accessKey} = req.query;

    //set the request promise options
    const options = {
        url: `https://api.fencer.io/v1.0/position/inside/${accessKey}`,
        headers: {
            'Authorization': FENCER_API_KEY,
            'Lat-Pos': latitude,
            'Lng-Pos': longitude
        },
        json:true
    }
    rp(options)
        .then(function(fencerData){
            //return if the user is in area
            const isUserInsideArea = fencerData.data.inside;

            //respond with JSON to ChatFuel
            const outputData = {
                set_attributes: {
                    userIsInArea: isUserInsideArea
                }
            }
            res.json(outputData);
        })
        .catch(function(err){
            res.json({status: false, message: "Error occurred"});
        })
});


//ChatFuel URL to fetch the questions
app.post('/getquestions', function(req, res){
    const {totalNoOfQuestions, score, totalAnsweredQuestions, lastQuestionId, lastTypedAnswer, answeredQuestions, providedAnswers} = req.body;

    var ChatFuelResponse = {
        "set_attributes": {
            "answeredQuestions": [],
            "providedAnswers": []
        },
        "messages": []
    }
    
    //get the next question
    var questionText = getQuestion(lastQuestionId);

    //update the answered questions array
    if(answeredQuestions == "null"){
        ChatFuelResponse.set_attributes.push();
    } else {
        ChatFuelResponse.set_attributes.answeredQuestions.push({lastQuestionId});
        ChatFuelResponse.set_attributes.providedAnswers.push({lastTypedAnswer});
    }
})

app.listen(8080);