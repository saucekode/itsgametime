const getStartedResponse = (userProfile) => {
    response = {
        "text": `Hello! Happy to have you on ITS GAMETIME! To get started, please select a menu option below:`,
        "quick_replies":[
          {
            "content_type":"text",
            "title":"Play Game",
            "payload":"play_game_menu"
          },
          {
            "content_type":"text",
            "title":"View Ranking",
            "payload":"view_ranking"
          },
          {
            "content_type":"text",
            "title":"Contact us",
            "payload":"contact_us"
          }
        ]    
    }

    return response;
}


const getDefaultResponse = () => {
    response = {
        "text": `Hi! I can't seem to understand your request. Please try any of the menu options below:`,
        "quick_replies":[
          {
            "content_type":"text",
            "title":"Play Game",
            "payload":"play_game_menu"
          },
          {
            "content_type":"text",
            "title":"View Ranking",
            "payload":"view_ranking"
          },
          {
            "content_type":"text",
            "title":"Contact us",
            "payload":"contact_us"
          }
        ]    
    }

    return response;
}

module.exports = {getStartedResponse, getDefaultResponse}