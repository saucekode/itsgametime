//SET DEFAULT QUICK REPLIES
const defaultQuickReplies = [
    {
      "content_type":"text",
      "title":"Play Game",
      "payload":"play_game_menu"
    },
    {
      "content_type":"text",
      "title":"View Ranking",
      "payload":"view_ranking"
    },
    {
      "content_type":"text",
      "title":"Contact us",
      "payload":"contact_us"
    }
];


//THIS IS THE RESPONSE FOR PLAY GAME SUBMENU
const playGameMenu = () => {
    response = {
        "text": `Please select a menu option below:`,
        "quick_replies":[
          {
            "content_type":"text",
            "title":"Live Game",
            "payload":"live_game"
          },
          {
            "content_type":"text",
            "title":"Play Trial",
            "payload":"play_trial"
          },
          {
            "content_type":"text",
            "title":"Show Main Menu",
            "payload":"main_menu"
          }
        ]    
    }

    return response;
}


//THIS IS THE MAIN MENU OPTIOn
const mainMenu = () => {
    response = {
        "text": `Here are the menu options"`,
        "quick_replies": defaultQuickReplies    
    }

    return response;
}

//THIS IS THE RESPONSE WHEN USER CLICKS GET STARTED BUTTON
const getStartedResponse = (userProfile) => {
    response = {
        "text": `Hello! Happy to have you on ITS GAMETIME! To get started, please select a menu option below:`,
        "quick_replies":defaultQuickReplies    
    }

    return response;
}


//THIS IS THE DEFAULT RESPONSE WHEN USER CANNOT DECIPHER WHAT THE USER WANTS
const getDefaultResponse = () => {
    response = {
        "text": `Hi! I can't seem to understand your request. Please try any of the menu options below:`,
        "quick_replies": defaultQuickReplies    
    }

    return response;
}

module.exports = {getStartedResponse, getDefaultResponse, playGameMenu}