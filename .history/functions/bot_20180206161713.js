const request = require('request');
const PAGE_ACCESS_TOKEN = process.env.PAGE_ACCESS_TOKEN;

const handleMessage = (sender_psid, received_message) => {
    let response;

    //check if the message is a quick reply
    if(received_message.quick_reply){
        //check if the payload is set in the quick reply
        if(received_message.quick_reply.payload){
            payload = received_message.quick_reply.payload;
            if(payload == "search_term"){
                response = {
                    "text": `You requested a search term`
                }
            } else if(payload == "send_yes"){
                response = {
                    "text": `Your payload is SEND YES`
                }
            }
        }
    }
    
    // Check if the message contains text
    else if (received_message.text) { 

        //test quixk replies
        response = {
            "text": "Here is a quick reply!",
            "quick_replies":[
              {
                "content_type":"text",
                "title":"Send Search Term",
                "payload":"search_term",
                "image_url":"https://www.gravatar.com/avatar/0b3ac738e74f7fbda25fca0f754b0aad?s=200"
              },
              {
                  "title":"Share your location",
                "content_type":"location"
              },
              {
                "content_type":"text",
                "title":"Send yes",
                "payload":"send_yes"
              }
            ]    
        }
    }
    //check if the message is an attachment
    else if (received_message.attachments) {
        
        //get the attachment type
        attachment_type = received_message.attachments[0].type;

        if(attachment_type == "location"){

        } else {

            // Gets the URL of the message attachment
            let attachment_url = payload.url;
            response = {
                "attachment": {
                    "type": "template",
                    "payload": {
                        "template_type": "generic",
                        "elements": [{
                            "title": "Is this the right picture?",
                            "subtitle": "Tap a button to answer.",
                            "image_url": attachment_url,
                            "buttons": [
                                {
                                    "type": "postback",
                                    "title": "Yes!",
                                    "payload": "yes",
                                },
                                {
                                    "type": "postback",
                                    "title": "No!",
                                    "payload": "no",
                                }
                            ],
                        }]
                    }
                }
            }

        }
    
    }
    
    // Sends the response message
    callSendAPI(sender_psid, response); 
}

const handlePostback = (sender_psid, received_postback) => {
    let response;
    
    // Get the payload for the postback
    let payload = received_postback.payload;
  
    // Set the response based on the postback payload
    if (payload === 'yes') {
      response = { "text": "Thanks!" }
    } else if (payload === 'no') {
      response = { "text": "Oops, try sending another image." }
    }
    // Send the message to acknowledge the postback
    callSendAPI(sender_psid, response);
}

const callSendAPI = (sender_psid, response) => {
    // Construct the message body
    let request_body = {
        "messaging_type": "RESPONSE",
        "recipient": {
            "id": sender_psid
        },
        "message": response
    }

    // Send the HTTP request to the Messenger Platform
    request({
        "uri": "https://graph.facebook.com/v2.6/me/messages",
        "qs": { "access_token": PAGE_ACCESS_TOKEN },
        "method": "POST",
        "json": request_body
    }, (err, res, body) => {
        if (!err) {
            console.log('message sent!')
        } else {
            console.error("Unable to send message:" + err);
        }
    }); 
}


module.exports = {handleMessage, handlePostback, callSendAPI}