const request = require('request');
const defaultResponses = require('./responses');

const PAGE_ACCESS_TOKEN = process.env.PAGE_ACCESS_TOKEN;

const getProfile = (psid) => {

    const userProfile = {
        "first_name": "",
        "last_name": "",
        "profile_pic":"",
        "locale": "en_US",
        "timezone": ""
    }

    const options = {
        method: 'GET',
        uri: `https://graph.facebook.com/v2.6/${psid}`,
        qs: {
            "fields": 'first_name,last_name,profile_pic,locale,timezone,gender',
            "access_token": PAGE_ACCESS_TOKEN 
        },
        json: true
    }

    rp(options).then(function(response){
        userProfile = response;
        return userProfile;
    }).catch(function(err){
        return userProfile;
    })
}

const handleMessage = (sender_psid, received_message) => {
    let response;

    //check if the message is a quick reply
    if(received_message.quick_reply){
        //check if the payload is set in the quick reply
        if(received_message.quick_reply.payload){
            payload = received_message.quick_reply.payload;
            if(payload == "play_game_menu"){
                response = {
                    "text": `You requested to Play a Game`
                }
            } else if(payload == "send_yes"){
                response = {
                    "text": `You requested to View Ranking`
                }
            } else if(payload == "contact_us"){
                response = {
                    "text": `You requested to Contact Us`
                }
            }
        }
    }
    
    // Check if the message contains text
    else if (received_message.text) { 
        //respond with the default menu
        userProfile = getProfile(sender_psid);
        response = defaultResponses.getDefaultResponse(userProfile);
    }
    //check if the message is an attachment
    else if (received_message.attachments) {
        
        //get the attachment type
        attachment_type = received_message.attachments[0].type;
        payload = received_message.attachments[0].payload;

        if(attachment_type == "location"){
            latitude = payload.coordinates.lat;
            longitude = payload.coordinates.long;
            response = {
                "text":`Your location is Latitude: ${latitude}, and Longitude: ${longitude}`
            }
        } else {

            // Gets the URL of the message attachment
            let attachment_url = payload.url;
            response = {
                "attachment": {
                    "type": "template",
                    "payload": {
                        "template_type": "generic",
                        "elements": [{
                            "title": "Is this the right picture?",
                            "subtitle": "Tap a button to answer.",
                            "image_url": attachment_url,
                            "buttons": [
                                {
                                    "type": "postback",
                                    "title": "Yes!",
                                    "payload": "yes",
                                },
                                {
                                    "type": "postback",
                                    "title": "No!",
                                    "payload": "no",
                                }
                            ],
                        }]
                    }
                }
            }

        }
    
    }
    
    // Sends the response message
    callSendAPI(sender_psid, response); 
}

const handlePostback = (sender_psid, received_postback) => {
    let response;
    
    // Get the payload for the postback
    let payload = received_postback.payload;
  
    // Set the response based on the postback payload
    if (payload === 'yes') {
      response = { "text": "Thanks!" }
    } else if (payload === 'no') {
      response = { "text": "Oops, try sending another image." }
    } 
    
    //if the user clicked the get started button
    else if(payload == 'new_get_started'){
        userProfile = getProfile(sender_psid);
        response = defaultResponses.getStartedResponse(userProfile);
    }
    // Send the message to acknowledge the postback
    callSendAPI(sender_psid, response);
}

const callSendAPI = (sender_psid, response) => {
    // Construct the message body
    let request_body = {
        "messaging_type": "RESPONSE",
        "recipient": {
            "id": sender_psid
        },
        "message": response
    }

    // Send the HTTP request to the Messenger Platform
    request({
        "uri": "https://graph.facebook.com/v2.6/me/messages",
        "qs": { "access_token": PAGE_ACCESS_TOKEN },
        "method": "POST",
        "json": request_body
    }, (err, res, body) => {
        if (!err) {
            console.log('message sent!')
        } else {
            console.error("Unable to send message:" + err);
        }
    }); 
}


module.exports = {handleMessage, handlePostback, callSendAPI}