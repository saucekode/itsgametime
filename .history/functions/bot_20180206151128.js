const request = require('request');
const PAGE_ACCESS_TOKEN = process.env.PAGE_ACCESS_TOKEN;

const handleMessage = (sender_psid, received_message) => {
    let response;
    
    // Check if the message contains text
    if (received_message.text) {    

        // Create the payload for a basic text message
        response = {
            "text": `You sent the message: "${received_message.text}". Now send me an image!`
        }
    }
    //check if the message is an attachment
    else if (received_message.attachments) {
        
        // Gets the URL of the message attachment
        let attachment_url = received_message.attachments[0].payload.url;
    
    }
    
    // Sends the response message
    callSendAPI(sender_psid, response); 
}

const handlePostback = (sender_psid, received_postback) => {
    
}

const callSendAPI = (sender_psid, response) => {
    // Construct the message body
    let request_body = {
        "recipient": {
        "id": sender_psid
        },
        "message": response
    }

    // Send the HTTP request to the Messenger Platform
    request({
        "uri": "https://graph.facebook.com/v2.6/me/messages",
        "qs": { "access_token": PAGE_ACCESS_TOKEN },
        "method": "POST",
        "json": request_body
    }, (err, res, body) => {
        if (!err) {
            console.log('message sent!')
        } else {
            console.error("Unable to send message:" + err);
        }
    }); 
}


module.exports = {handleMessage, handlePostback, callSendAPI}