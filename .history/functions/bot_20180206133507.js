module.exports = function(rp, PAGE_ACCESS_TOKEN){
    // Handles messages events
    function handleMessage(sender_psid, received_message) {
        let response;
        
        // Check if the message contains text
        if (received_message.text) {    
    
            // Create the payload for a basic text message
            response = {
                "text": `You sent the message: "${received_message.text}". Now send me an image!`
            }
        }  
        
        // Sends the response message
        callSendAPI(sender_psid, response); 
    }
    
    // Handles messaging_postbacks events
    function handlePostback(sender_psid, received_postback) {
    
    }
    
    // Sends response messages via the Send API
    function callSendAPI(sender_psid, response) {
        // Construct the message body
        let request_body = {
            "recipient": {
            "id": sender_psid
            },
            "message": response
        }

        // Send the HTTP request to the Messenger Platform
        let options = {
            "url": "https://graph.facebook.com/v2.6/me/messages",
            "qs": { "access_token": PAGE_ACCESS_TOKEN },
            "method": "POST",
            "json": request_body
        };
        rp(options).then(function(body){
            console.log('message sent!')
        }).catch(function(err){
            if(err){
                console.error("Unable to send message:" + err);
            }
        })
    }
}