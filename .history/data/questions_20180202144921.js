module.exports = {
    questions: [
      "Who has more Super Bowl starts?",
      "Who is the oldest Quarterback to have won a Super Bowl?",
      "Who is the youngest Quarterback to have won a Super Bowl?", 
      "Who is the only retired quarterback with two Super Bowl MVPs to not be in the Hall of Fame?",
      "Who has thrown for the most yards in a single Super Bowl?",
      "Which Quarterback threw the longest Touchdown pass in a Super Bowl?",
      "Which Quarterback has the highest pass completion rate in Super Bowls?"
    ]
}