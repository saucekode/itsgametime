module.exports = {
    answer_options: [
        ["Tom Brady","John Elway"],
        ["Peyton Manning","Johnny Unitas"],
        ["Ben Roethlisberger","Dan Marino"],
        ["Jim Plunkett","Bart Starr"],
        ["Kurt Warner","Brett Favre"],
        ["Jake Delhomme","Doug Williams"],
        ["Troy Aikman","Joe Montana"]
    ],
}