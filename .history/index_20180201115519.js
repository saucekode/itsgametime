const express = require('express');
const app = express();
const bodyParser = require('body-parser');

//Serve static files
app.use(express.static('public'));

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

app.get('/', function(req, res){
    res.send(process.env.FENCER_API_KEY);
});

app.listen(3000);