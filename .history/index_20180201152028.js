require('dotenv').load();
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const rp = require('request-promise');

//Serve static files
app.use(express.static('public'));

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

//this is the homepage
app.get('/', function(req, res){
    res.send('My Paddy, ITS GAME TIME!!!');
})

//this routes to the fencer API, to check if the latitude, and longitude are inside the area
app.get('/geofence', function(req, res){
    //get fencer api key
    const FENCER_API_KEY = process.env.FENCER_API_KEY;
    //get the latitude, longitude, and accesskey from query params
    const {latitude, longitude, accessKey} = req.query;

    //set the request promise options
    const options = {
        url: `https://api.fencer.io/v1.0/position/inside/${accessKey}`,
        headers: {
            'Authorization': FENCER_API_KEY,
            'Lat-Pos': latitude,
            'Lng-Pos': longitude
        },
        json:true
    }
    rp(options)
        .then(function(fencerData){
            //return if the user is in area
            const isUserInsideArea = fencerData.data.inside;

            //respond with JSON to ChatFuel
            const outputData = {
                set_attributes: {
                    userIsInArea: isUserInsideArea
                }
            }
            res.send(result);
        })
        .catch(function(err){
            res.json({status: false, message: "Error occurred"});
        })
});

app.listen(8080);