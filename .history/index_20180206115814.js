//load the .env file
require('dotenv').load();
//load express framework
const express = require('express');
const app = express();
//load body parser
const bodyParser = require('body-parser');
//load request promise
const rp = require('request-promise');

//load the logger
const log = require('simple-node-logger').createSimpleFileLogger('itsgametime.log');

//get questions, answers, and correct answers
var questions = require('data/questions');
var answers = require('data/answers');
var correct_answers = require('data/correct_answers');

//Serve static files
app.use(express.static('public'));

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

//this is the homepage
app.get('/', function(req, res){
    res.send('My Paddy, ITS GAME TIME!!!');
})

//this routes to the fencer API, to check if the latitude, and longitude are inside the area
app.get('/geofence', function(req, res){
    //get fencer api key
    const FENCER_API_KEY = process.env.FENCER_API_KEY;
    //get the latitude, longitude, and accesskey from query params
    const {latitude, longitude, accessKey} = req.query;

    //set the request promise options
    const options = {
        url: `https://api.fencer.io/v1.0/position/inside/${accessKey}`,
        headers: {
            'Authorization': FENCER_API_KEY,
            'Lat-Pos': latitude,
            'Lng-Pos': longitude
        },
        json:true
    }
    rp(options)
        .then(function(fencerData){
            //return if the user is in area
            const isUserInsideArea = fencerData.data.inside;

            //respond with JSON to ChatFuel
            const outputData = {
                set_attributes: {
                    userIsInArea: isUserInsideArea
                }
            }
            res.json(outputData);
        })
        .catch(function(err){
            res.json({status: false, message: "Error occurred"});
        })
});


function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

function generateRandomInteger(quesLength){
    
}

function getNextQuestion(answeredQuestions){
    const noOfQuestions = questions.length;
    const answeredQuestionsLength = answeredQuestions.length;
    const questionId = null;
    if(answeredQuestionsLength < noOfQuestions){
        var copyQuestions = questions;
        if(answeredQuestionsLength > 0){
            for(i = 0; i < answeredQuestionsLength; i++){
                copyQuestions.splice(answeredQuestions[i], 1);
            }
        }
    }

    return questionId;
}

function getNewScore(score, lastQuestionId, lastTypedAnswer){
    if(answers[lastQuestionId] == lastTypedAnswer){
        score++;
    }
    return score;
}

function processQuickReplies(replies){
    var repliesData = [];
    for(i = 0; i < replies.length; i++){
        repliesData.push({title: replies[i], block_names: ["FetchQuestion"]});
    }
    return repliesData;
}

//ChatFuel URL to fetch the questions
app.post('/getquestions', function(req, res){
    const {totalNoOfQuestions, score, totalAnsweredQuestions, lastQuestionId, lastTypedAnswer, answeredQuestions} = req.body;

    //check if answered question is recently initialized
    if(answeredQuestions == "null"){
        answeredQuestions = [];
    } else {
        answeredQuestions.push(lastQuestionId);
    }

    var ChatFuelResponse = {
        "set_attributes": {
            "answeredQuestions": answeredQuestions,
            "score": score
        },
        "messages": []
    }

    //increment the total answered questions
    if(lastQuestionId !== "null"){
        //increment total answered questions
        totalAnsweredQuestions++;

        //determine the new score
        const newScore = getNewScore(score, lastQuestionId, lastTypedAnswer);
        ChatFuelResponse.set_attributes.score = newScore;
    }
    
    //get the next question
    var questionId = getNextQuestion(answeredQuestions);
    if(questionId !== null) {
        const questionText = questions[questionId];
        const quickReplies = processQuickReplies(answers[questionId]);
        const newQuestion = {
            "text":  questionText,
            "quick_replies": quickReplies
        }
        ChatFuelResponse.messages.push(newQuestion);
    } 
    else {
        //no more question. Send a default response
        var defaultResponse = {
            text: "You have completed the quiz",
            block_names: ["FetchQuestion"]
        };
        ChatFuelResponse.messages.push(defaultResponse);
    }


    //send the response
    res.json(ChatFuelResponse);
})



// Adds support for GET requests to our webhook
app.get('/webhook', (req, res) => {
    
    // Your verify token. Should be a random string.
    let VERIFY_TOKEN = "MrKulikuli100$"
    
    // Parse the query params
    let mode = req.query['hub.mode'];
    let token = req.query['hub.verify_token'];
    let challenge = req.query['hub.challenge'];
    
    // Checks if a token and mode is in the query string of the request
    if (mode && token) {
    
    // Checks the mode and token sent is correct
    if (mode === 'subscribe' && token === VERIFY_TOKEN) {
        
        // Responds with the challenge token from the request
        console.log('WEBHOOK_VERIFIED');
        res.status(200).send(challenge);
    
    } else {
        // Responds with '403 Forbidden' if verify tokens do not match
        res.sendStatus(403);      
    }
    }
});

// Creates the endpoint for our webhook 
app.post('/webhook', (req, res) => {  
    
    let body = req.body;
   
    // Checks this is an event from a page subscription
    if (body.object === 'page') {
   
       // Iterates over each entry - there may be multiple if batched
       body.entry.forEach(function(entry) {
   
         // Gets the message. entry.messaging is an array, but 
         // will only ever contain one message, so we get index 0
         let webhook_event = entry.messaging[0];
         console.log(webhook_event);
       });
   
       // Returns a '200 OK' response to all requests
       res.status(200).send('EVENT_RECEIVED');
    } else {
       // Returns a '404 Not Found' if event is not from a page subscription
       res.sendStatus(404);
    }
   
});



app.listen(8080);